#!/bin/bash
set -e

HE_DIR=$1
HOST_NAME=$2

PROPERTY_FILE=./bcbsne_env.properties

function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`
   cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
   echo $PROP_VALUE
}

echo ""
echo "********** BCBSNE SH : bcbsne_security_audit_report_configure.sh start on ${HOST_NAME} **********"

    echo "Start configuring  bcbsne_security_audit_report job"

        groupReportCfgFile=$HE_DIR/bcbsne-security-audit-report/resources/config/bcbsne-group-audit-report-config.json
        chmod -R 755 $groupReportCfgFile

        sed -i -e "s|#HE_DIR#|${HE_DIR}|g" $groupReportCfgFile

        extractStartEndTime=$(date +"%Y-%m-%dT%T.%3NEDT")
        echo "extractStartEndTime: ${extractStartEndTime}"

        sed -i -e "s|SSSS-MM-DDTHH:MM:SS.SSSZEDT|${extractStartEndTime}|g" $groupReportCfgFile
        sed -i -e "s|EEEE-MM-DDTHH:MM:SS.SSSZEDT|${extractStartEndTime}|g" $groupReportCfgFile


        configStagingDir=$(grep "extractConfigStagingDir" $HE_DIR/etc/com.healthedge.he.common.extract.configuration.cfg | sed -e "s/extractConfigStagingDir=//g")
        echo "configStagingDir: ${configStagingDir}"
        mkdir -p $configStagingDir

        extractConfigRequestDir=$(grep "extractConfigRequestDir" $HE_DIR/etc/com.healthedge.he.common.extract.configuration.cfg | sed -e "s/extractConfigRequestDir=//g")
        echo "extractConfigRequestDir: ${extractConfigRequestDir}"
        mkdir -p $extractConfigRequestDir

        jobName=$(grep "jobName" $groupReportCfgFile | sed -e "s/jobName//g;s/[\", :]//g")
        
        # "s/-/\./g"           - replace '-' with '.'
        # "s/\(.*\)/\L\1/g"    - convert to lower case
        configJobName=$(sed -e "s/-/\./g;s/\(.*\)/\L\1/g" <<< "$jobName")
        
        cfgFile=$HE_DIR/etc/com.healthedge.he.common.extract.${configJobName}.cfg
        if [ ! -f "$cfgFile" ]
        then
        	echo "Dropping bcbsne_security_audit_report job configuration json ${groupReportCfgFile} to ${extractConfigRequestDir}"
        	cp $groupReportCfgFile $extractConfigRequestDir/bcbsne-group-audit-report-config.json
        else
            echo "${jobName} is already configured"
        fi

    echo "End configuring  bcbsne_security_audit_report job"
    
echo "********** BCBSNE SH : bcbsne_security_audit_report_configure.sh end on ${HOST_NAME} **********"
echo ""
